package com.ddm.product.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.ddm.product.dto.ResponseTemplateDto;
import com.ddm.product.dto.request.ScheduledTransactionRequestDto;
import com.ddm.product.service.ScheduledTransactionService;
import com.ddm.product.utils.ResponseBuilder;

@RestController
public class ScheduledTransactionController {
	
	@Autowired
	private ScheduledTransactionService service;
	
	@PostMapping()
	public ResponseEntity<ResponseTemplateDto<Boolean>> addScheduledTransaction(
			@RequestBody final ScheduledTransactionRequestDto requestObj) {
		return ResponseBuilder.created(this.service.addScheduledTransaction(requestObj));
	}

}
