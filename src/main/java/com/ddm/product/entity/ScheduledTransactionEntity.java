package com.ddm.product.entity;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.UUID;

import javax.annotation.Generated;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.ddm.product.audit.Auditable;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Builder
@Getter
@Setter
@Table(name = "ScheduledTransaction")
@Entity
@NoArgsConstructor
@AllArgsConstructor
public class ScheduledTransactionEntity extends Auditable<UUID> {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column
	private String transactionAlias;
	
	@Column
	private LocalDate dueDate;
	
	@Column
	private Integer installments; 
	
	@Column
	private BigDecimal dueAmount;

}
