package com.ddm.product.service;

import com.ddm.product.dto.request.ScheduledTransactionRequestDto;

public interface ScheduledTransactionService {
	
	public Boolean addScheduledTransaction(ScheduledTransactionRequestDto requestDto);

}
