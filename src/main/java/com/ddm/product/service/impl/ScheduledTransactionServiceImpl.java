package com.ddm.product.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ddm.product.dto.request.ScheduledTransactionRequestDto;
import com.ddm.product.entity.ScheduledTransactionEntity;
import com.ddm.product.service.GenericCrudOperations;
import com.ddm.product.service.ScheduledTransactionService;

@Service
public class ScheduledTransactionServiceImpl implements ScheduledTransactionService{
	
	@Autowired
	private GenericCrudOperations<ScheduledTransactionEntity> repoService;

	@Override
	public Boolean addScheduledTransaction(ScheduledTransactionRequestDto requestDto) {
		repoService.addOneRecord(null);
		return Boolean.TRUE;
	}

}
