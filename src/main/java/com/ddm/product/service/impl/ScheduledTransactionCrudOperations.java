package com.ddm.product.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ddm.product.entity.ScheduledTransactionEntity;
import com.ddm.product.exception.ResourceNotFoundException;
import com.ddm.product.repository.ScheduledTransactionRepository;
import com.ddm.product.service.GenericCrudOperations;
import com.ddm.product.utils.SecurityUtils;

@Service
public class ScheduledTransactionCrudOperations implements GenericCrudOperations<ScheduledTransactionEntity>{
	
	@Autowired
	private ScheduledTransactionRepository repositoryService;

	@Override
	public ScheduledTransactionEntity addOneRecord(ScheduledTransactionEntity request) {
		return this.repositoryService.save(request);
	}

	@Override
	public List<ScheduledTransactionEntity> addAllRecords(List<ScheduledTransactionEntity> requestList) {
		return this.repositoryService.saveAll(requestList);
	}

	@Override
	public void deleteOneRecord(Long id) {
		this.repositoryService.deleteById(id);		
	}

	@Override
	public List<ScheduledTransactionEntity> getAllRecords() {
		return this.repositoryService.findByUserId(SecurityUtils.getUserId());
	}

	@Override
	public ScheduledTransactionEntity getOneRecord(Long id) {
		return this.repositoryService.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException("ScheduledTransactionEntity", "id", id));
	}

}
