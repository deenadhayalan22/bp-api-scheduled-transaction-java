package com.ddm.product;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BpApiScheduledTransactionJavaApplication {

	public static void main(String[] args) {
		SpringApplication.run(BpApiScheduledTransactionJavaApplication.class, args);
	}

}
