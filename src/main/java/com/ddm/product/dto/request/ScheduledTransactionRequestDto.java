package com.ddm.product.dto.request;

import java.math.BigDecimal;
import java.time.LocalDate;

public class ScheduledTransactionRequestDto {
	
	private String transactionName;
	
	private LocalDate dueDate;
	
	private Integer installments;
	
	private BigDecimal dueAmount;
	

}
