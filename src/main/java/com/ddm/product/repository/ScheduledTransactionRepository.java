package com.ddm.product.repository;

import java.util.List;
import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ddm.product.entity.ScheduledTransactionEntity;

@Repository
public interface ScheduledTransactionRepository extends JpaRepository<ScheduledTransactionEntity, Long>{
	
	List<ScheduledTransactionEntity> findByUserId(UUID userId);

}
